# frozen_string_literal: true

class CourseDecorator < ApplicationDecorator
  delegate_all

  def disciplines_name
    disciplines.map { |discipline| discipline.name.to_s }.join(', ')
  end
end
