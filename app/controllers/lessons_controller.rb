# frozen_string_literal: true

class LessonsController < ApplicationController
  before_action :lesson, :sections

  private

  def sections
    @sections = course.sections.order(:position)
  end

  def course
    @course = Course.find(params[:course_id])
  end

  def lesson
    @lesson = course.lessons.find(params[:id]) if params[:id].present?
  end
end
