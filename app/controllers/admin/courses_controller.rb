# frozen_string_literal: true

class Admin::CoursesController < Admin::BaseController
  add_breadcrumb 'Курсы', :admin_courses_path
  before_action :set_course, only: %i[edit update destroy]

  def index
    @courses = Course.order(id: :desc).page(params[:page]).per(5)
  end

  def new
    add_breadcrumb 'Новый Курс', new_admin_course_path
    @course = Course.new
    build_sections
    @course.id = 1
  end

  def create
    @course = Course.new(course_params)
    if @course.save
      redirect_to admin_courses_path, notice: 'Курс успешно создан'
    else
      flash.now[:alert] = 'Не удалось создать курс'
      add_breadcrumb 'Новый Курс', new_admin_course_path
      build_sections
      render :new
    end
  end

  def edit
    build_sections
    add_breadcrumb "Изменить #{@course.name}", [:edit, :admin, @course]
  end

  def update
    # @course = Course.find(1)
    # byebug

    if @course.update(course_params)
      redirect_to admin_courses_path, notice: 'Курс успешно изменен'
    else
      build_sections
      flash.now[:alert] = 'Не удалось изменить курс'
      add_breadcrumb "Изменить #{@course.name}", [:edit, :admin, @course]
      render :edit
    end
  end

  def destroy
    if @course.destroy
      redirect_to admin_courses_path, notice: 'Курс успешно удален'
    else
      redirect_to admin_courses_path, alert: 'Не удалось удалить курс'
    end
  end

  private

  def course_params
    params.require(:course).permit(:name, :teacher_id, { discipline_ids: [] }, :description,
                                   sections_attributes: %i[_destroy id name description position])
  end

  def build_sections
    @course.sections.build if @course.sections.empty?
  end

  def set_course
    @course = Course.find(params[:id])
  end

  def set_active_main_menu_item
    @main_menu[:courses][:active] = true
  end
end
