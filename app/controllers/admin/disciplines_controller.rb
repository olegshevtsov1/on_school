# frozen_string_literal: true

class Admin::DisciplinesController < Admin::BaseController
  add_breadcrumb 'Дисциплины', :admin_disciplines_path
  before_action :set_discipline, only: %i[edit update destroy]

  def index
    @disciplines = Discipline.order(id: :desc).page(params[:page])
  end

  def new
    add_breadcrumb 'Новая Дисциплина', new_admin_discipline_path
    @discipline = Discipline.new
  end

  def create
    @discipline = Discipline.new(discipline_params)
    if @discipline.save
      redirect_to admin_disciplines_path, notice: 'Дисциплина успешно создана'
    else
      flash.now[:alert] = 'Не удалось создать дисциплину'
      add_breadcrumb 'Новая Дисциплина', new_admin_discipline_path
      render :new
    end
  end

  def edit
    add_breadcrumb "Изменить #{@discipline.name}", [:edit, :admin, @discipline]
  end

  def update
    if @discipline.update(discipline_params)
      redirect_to admin_disciplines_path, notice: 'Дисциплина успешно изменена'
    else
      flash.now[:alert] = 'Не удалось изменить дисциплину'
      add_breadcrumb "Изменить #{@discipline.name} ", [:edit, :admin, @discipline]
      render :edit
    end
  end

  def destroy
    if @discipline.destroy
      redirect_to admin_disciplines_path, notice: 'Дисциплина успешно удалена'
    else
      redirect_to admin_disciplines_path, alert: 'Не удалось удалить дисциплину'
    end
  end

  private

  def discipline_params
    params.require(:discipline).permit(:name)
  end

  def set_discipline
    @discipline = Discipline.find(params[:id])
  end

  def set_active_main_menu_item
    @main_menu[:disciplines][:active] = true
  end
end
