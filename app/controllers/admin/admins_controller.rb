# frozen_string_literal: true

class Admin::AdminsController < Admin::BaseController
  add_breadcrumb 'Админы', :admin_admins_path
  before_action :set_admin, only: %i[edit update destroy]

  def index
    @admins = Admin.order(id: :desc).page(params[:page]).per(5)
  end

  def new
    add_breadcrumb 'Новый Админ', new_admin_admin_path
    @admin = Admin.new
  end

  def create
    @admin = Admin.new(admin_params)
    if @admin.save
      redirect_to admin_admins_path, notice: 'Админ успешно создан'
    else
      flash.now[:alert] = 'Не удалось создать админа'
      add_breadcrumb 'Новый Админ', new_admin_admin_path
      render :new
    end
  end

  def edit
    add_breadcrumb "Изменить #{@admin.first_name} #{@admin.last_name}", [:edit, :admin, @admin]
  end

  def update
    if @admin.update(admin_params)
      redirect_to admin_admins_path, notice: 'Админ успешно изменен'
    else
      flash.now[:alert] = 'Не удалось изменить админа'
      add_breadcrumb "Изменить #{@admin.first_name} #{@admin.last_name}", [:edit, :admin, @admin]
      render :edit
    end
  end

  def destroy
    if @admin.destroy
      redirect_to admin_admins_path, notice: 'Админ успешно удален'
    else
      redirect_to admin_admins_path, alert: 'Не удалось удалить админа'
    end
  end

  private

  def admin_params
    params.require(:admin).permit(:email, :first_name, :last_name, :password)
  end

  def set_admin
    @admin = Admin.find(params[:id])
  end

  def set_active_main_menu_item
    @main_menu[:admins][:active] = true
  end
end
