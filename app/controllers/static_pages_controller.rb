# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def contact
  end
end
