# frozen_string_literal: true

module AdminHelper
  def render_flash
    # %w[notice alert]
    #     .select { |type| flash_block(type) }
    #     .map { |type| flash_block(type) }
    #     .join.html_safe

    return unless [:notice].present?

    %w[notice]
      .select { |type| flash_block(type) }
      .map { |type| flash_block(type) }
      .join.html_safe

    return unless flash[:alert].present?

    %w[alert]
      .select { |type| flash_block(type) }
      .map { |type| flash_block(type) }
      .join.html_safe
  end

  private

  def flash_block(type)
    flash_classes = { notice: 'alert alert-success', alert: 'alert alert-danger' }
    content_tag(:div, class: flash_classes[type.to_sym]) do
      content_tag('button', 'x', class: 'close clear', 'data-dismiss' => 'alert') + flash[type.to_sym]
    end
  end
end
