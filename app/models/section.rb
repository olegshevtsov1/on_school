# frozen_string_literal: true

class Section < ApplicationRecord
  belongs_to :course
  has_many :lessons, dependent: :delete_all

  validates :name, presence: true
  validates :description, presence: true
  validates :course_id, presence: true

  acts_as_list
end
