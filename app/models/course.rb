# frozen_string_literal: true

class Course < ApplicationRecord
  belongs_to :teacher
  has_many :lessons
  has_many :tinymce_images, as: :owner

  has_many :course_disciplines, dependent: :delete_all
  has_many :disciplines, through: :course_disciplines
  has_many :sections, dependent: :delete_all

  validates :name, presence: true
  validates :description, presence: true
  validates :disciplines, presence: true

  accepts_nested_attributes_for :sections, reject_if: :all_blank, allow_destroy: true
end
