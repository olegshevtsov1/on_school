# frozen_string_literal: true

class CourseDiscipline < ApplicationRecord
  belongs_to :discipline
  belongs_to :course
end
