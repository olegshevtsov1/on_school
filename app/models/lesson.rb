# frozen_string_literal: true

class Lesson < ApplicationRecord
  belongs_to :course
  belongs_to :section
  has_many :tinymce_images, as: :owner

  validates :name, presence: true
  validates :description, presence: true

  acts_as_list

  def self.reorder(order_params)
    order_params.each_with_index do |id, index|
      # Lesson.find(id).update!(position: index + 1)
      Lesson.find(id).set_list_position(index, true)
    end
  end
end
