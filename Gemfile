# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'

gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'

gem 'jquery-rails'
gem 'jquery-ui-rails'

gem 'bootstrap-sass'
gem 'bootstrap-select-rails'

gem 'cocoon'
gem 'simple_form'
gem 'slim-rails'

# gem 'russian', github: 'yaroslav/russian'

gem 'bootstrap-kaminari-views'
gem 'breadcrumbs_on_rails'
gem 'kaminari'

gem 'draper'
# gem 'ransack'

gem 'devise'

gem 'state_machines-activerecord'
# gem 'after_commit_queue'

gem 'mini_magick'

gem 'acts_as_list'

gem 'rubocop', require: false

gem 'turbolinks'

# gem 'sidekig'
# gem 'sidekig-failures'
# gem 'sidekig-limit-fetch'
# gem 'sinatra', github: 'sintra/sinatra' , require :false

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'carrierwave', '~> 2.0'
gem 'tinymce-rails', '~> 4.0'
gem 'tinymce-rails-imageupload', '~> 4.0.17.beta.3'

group :development, :test do
  gem 'rspec-rails'
  gem 'pry', '~> 0.12.2'
  # gem 'pry-rails'
  # gem 'pry-theme'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: :mri
  # Adds support for Capybara system testing and selenium driver
  # gem 'capybara', '~> 2.13'
  # gem 'selenium-webdriver'
end

group :development do
  gem 'priscilla'

  gem 'better_errors'
  gem 'binding_of_caller'
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  # gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'capistrano'
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-scm-copy'
  gem 'capistrano3-puma'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

group :test do
  gem 'shoulda-matchers', require: false

  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'timecop'
  # gem 'webmock'
  # gem 'vcr'
  gem 'simplecov'
end
