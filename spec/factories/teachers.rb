FactoryBot.define do
  factory :teacher do
    first_name { "MyString" }
    last_name { "MyString" }
    description { "MyString" }
    text { "MyString" }
  end
end
