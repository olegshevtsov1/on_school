FactoryBot.define do
  factory :section do
    name { "MyString" }
    description { "MyText" }
    position { "MyString" }
    course { nil }
  end
end
