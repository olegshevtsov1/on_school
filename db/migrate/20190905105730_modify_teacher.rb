class ModifyTeacher < ActiveRecord::Migration[5.1]
  def change
    change_table :teachers do |t|
      t.change :description, :text
    end
    remove_column :teachers, :text
  end
end
