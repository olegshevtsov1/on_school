class AddFieldsToCourses < ActiveRecord::Migration[5.1]
  def change
    add_column :courses, :description, :text
  end
  change_table :courses do |t|
    t.references :teacher, foreign_key: true
    t.references :discipline, foreign_key: true
  end
end
