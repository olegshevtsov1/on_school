class CreateLessons < ActiveRecord::Migration[5.1]
  def change
    create_table :lessons do |t|
      t.string :name, null: false
      t.string :description
      t.belongs_to :course, index: true
      t.timestamps
    end
  end
end
