class AddSectionIdToLesson < ActiveRecord::Migration[5.1]
  change_table :lessons do |t|
    t.references :section, foreign_key: true
  end
end
