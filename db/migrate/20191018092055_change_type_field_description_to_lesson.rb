class ChangeTypeFieldDescriptionToLesson < ActiveRecord::Migration[5.1]
  def change
    change_table :lessons do |t|
      t.change :description, :text
    end
  end
end
