# frozen_string_literal: true

if Lesson.count.zero?
  puts 'Seeding Lesson'

  %w(1 2).each do |n|
    Lesson.create!(name: "Lesson#{n}", course_id: 1, description: "#{n}")
  end
end