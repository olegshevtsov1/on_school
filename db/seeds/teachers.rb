# frozen_string_literal: true

if Teacher.count.zero?
  puts 'Seeding Teachers'
  5.times{ |n| Teacher.create!(first_name: "John", last_name: "Dow#{n}", description: 'Second Teacher')}
  Teacher.create!(first_name: 'Галина', last_name: 'Кряжева', description: 'Main Teacher')
  Teacher.create!(first_name: 'John', last_name: 'Dow', description: 'Second Teacher')
  Teacher.create!(first_name: 'Jane', last_name: 'Dow', description: 'Third Teacher')
end
