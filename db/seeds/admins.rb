# frozen_string_literal: true

if Admin.count.zero?
  puts 'Seeding Admins'
  Admin.create!(email: 'john@dow.com', first_name: 'Jon', last_name: 'Dow', password: '123456')
end
