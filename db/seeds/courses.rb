# frozen_string_literal: true

if Course.count.zero?
  puts 'Seeding Courses'

  %w(Курс1 Курс2).each do |name|
    Course.create!(name: name, teacher_id: 1, description: '1')
  end
end