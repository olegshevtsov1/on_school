# frozen_string_literal: true

if CourseDiscipline.count.zero?
  puts 'Seeding CourseDisciplines'

  [1,2].each do |id|
    CourseDiscipline.create!(course_id: id, discipline_id: 1)
    CourseDiscipline.create!(course_id: id, discipline_id: 2)
  end
end